Express:            is the framework.
Ejs:                is the templating engine.
Mongoose:           is object modeling for our MongoDB database.
Passport stuff:     will help us authenticating with different methods.
Connect-flash:      allows for passing session flashdata messages.
Bcrypt-nodejs:      gives us the ability to hash the password.