module.exports = function(app, passport) {
    app.post('/signup', function(req, res) {
        passport.authenticate('local-signup', function(err, user){
            if(err){
                res.json(err);
            }
            if(user) {
                res.json({
                    data: user,
                    message: 'signup success'
                });
            } else {
                res.json({
                    data: null,
                    message: 'signup error'
                });
            }
        })(req, res);
    });
    
    app.post('/login', function(req, res) {
        passport.authenticate('local-login', function(err, user){
            if(err){
                res.json(err);
            }
            if(user) {
                res.json({
                    data: user,
                    message: 'login success'
                });
            } else {
                res.json({
                    data: null,
                    message: 'email or password invalid'
                });
            }
        })(req, res);
    });
};

