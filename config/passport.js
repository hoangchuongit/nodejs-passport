const LocalStrategy = require('passport-local').Strategy;
const User = require('../app/models/user');

module.exports = function(passport) {
        
    // =========================================================================
    // passport session setup ==================================================
    // =========================================================================
    // required for persistent login sessions
    // passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });
    
    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
           done(err, user); 
        });
    });
    
    //==========================================================================
    //LOCAL SINGUP =============================================================
    passport.use('local-signup', new LocalStrategy({
        // by default, local strategy uses username and password, we will overrider with email
        usernameField: 'email',
        passwordField: 'password',
        //passReqToCallBack: true // allows us to pass back the entire request to the callback
    },
    function(email, password, done) {
        console.log(done);
        // async
        // User.findOne wont fire unless data send back
        process.nextTick(function() {
            // find a user whose email is the same as the forms email
            // we are checking to see if the user trying to login already exists
            User.findOne({ 'local.email': email }, function(err, user) {
                // if there are any errors, return the error
                if(err) {
                    return done(err);
                }
                
                // check to see if theres already a user with that email
                if(user) {
                    return done(null, false);
                } else {
                    // if there is no user with that email
                    // create the user
                    var user = new User();
                    
                    // set the user's local credentials
                    user.local.email = email;
                    user.local.password = user.generateHash(password);
                    
                    // save the user
                    user.save(function(err) {
                       if(err) {
                           throw err;
                       } 
                       return done(null, user);
                    });
                }
            });
        });
    }));
    
    
    //==========================================================================
    //LOCAL LOGIN ==============================================================
    //==========================================================================
    // we are using named strategies since we have one for login and one for signup
    // by default, if there was no name, it would just be called 'local'
    passport.use('local-login', new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    },
    function(email, password, done){
        User.findOne({ 'local.email' : email }, function(err, user) {
            if (err) {
                return done(err);
            }
            if (!user) {
                return done(null, false);
            }
            if (!user.validPassword(password)) {
               return done(null, false); 
            }
            return done(null, user);
        });
    }));
};