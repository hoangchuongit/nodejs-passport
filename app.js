// set up ========================================================
// get all the tools we need
const express = require('express');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const passport = require('passport');

const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const session = require('express-session');

dotenv.load({path: '.env'});

const app = express();
const port = process.env.PORT || 8080;


// configuration =================================================
mongoose.connect(process.env.MONGO_HOST, function(err) {
    if (err) {
        console.log(err);
    } else {
        console.log('connect db successfully');
    }
});

app.use(cookieParser()); // read cookies (needed for auth) // cookieParser must be placed on bodyParser
app.use(bodyParser()); // get information from html forms not handle file (using:formidable)

app.use(session({ secret: 'imustlearnusingnode' })); // session secret using for decode


app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions


// routes ======================================================================
require('./app/routes.js')(app, passport); // load our routes and pass in our app and fully configured passport
require('./config/passport')(passport); // pass passport for configuration

// launch ========================================================
app.listen(port, function(err) {
    console.log('The magic happens on port ' + port);
});